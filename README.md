# ml-mozhi

Malayalam input method — ‘mozhi’ transliteration scheme.

## Enhancements

This project contains few major changes from upstream
1. Fixes spurious ZWNJ (`U+200C`) in a [few combinations](https://savannah.nongnu.org/bugs/?59681)
2. Joiner chillus by default (n_ → ൻ ). Use double underscore for atomic chillus
3. Additional characters (dotreph (R_), ൕ, ൔ,  ...)

## Installation

1. Install `m17n-db` if not already
2. Download [ml-mozhi.mim](ml-mozhi.mim) (say, to `~/Downloads`)
3. `sudo install -m 0644 ~/Downloads/ml-mozhi.mim /usr/share/m17n/`
4. Restart `ibus-daemon`, using command `ibus-deamon -dr` or via system tray menu or logout/login.
